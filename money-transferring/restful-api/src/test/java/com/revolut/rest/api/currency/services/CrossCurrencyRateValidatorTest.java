package com.revolut.rest.api.currency.services;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CrossCurrencyRateValidatorTest {
    private CrossCurrencyRateValidator crossCurrencyRateValidator;

    @Before
    public void setUp() throws Exception {
        crossCurrencyRateValidator = new CrossCurrencyRateValidator();
    }

    @Test
    public void negativeBalanceIsNotAllowed() {
        assertFalse(crossCurrencyRateValidator.isValidCrossRate(BigDecimal.valueOf(-1)));
    }

    @Test
    public void zeroBalanceIsNotAllowed() {
        assertFalse(crossCurrencyRateValidator.isValidCrossRate(BigDecimal.ZERO));
    }

    @Test
    public void BalanceExceededMaxIsNotAllowed() {
        assertFalse(crossCurrencyRateValidator.isValidCrossRate(BigDecimal.valueOf(2_000_000_000)));
    }

    @Test
    public void positiveBalanceIsAllowed() {
        assertTrue(crossCurrencyRateValidator.isValidCrossRate(BigDecimal.valueOf(0.01)));
        assertTrue(crossCurrencyRateValidator.isValidCrossRate(BigDecimal.valueOf(1)));
        assertTrue(crossCurrencyRateValidator.isValidCrossRate(BigDecimal.valueOf(1_999_999_999.99)));
    }
}