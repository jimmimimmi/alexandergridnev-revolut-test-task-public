package com.revolut.rest.api.account.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.account.dto.AccountDto;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class AccountConverterTest {
    private AccountConverter converter;
    private int id;
    private int currencyId;
    private BigDecimal balance;

    @Before
    public void setUp() throws Exception {
        converter = new AccountConverter();

        Random random = new Random();
        id = random.nextInt();
        currencyId = random.nextInt();
        balance = BigDecimal.valueOf(random.nextLong());
    }

    @Test
    public void convertToDTO() {
        AccountDto dto = converter.convertToDTO(new Account(id, currencyId, balance));

        assertEquals(balance, dto.getBalance());
        assertEquals(id, dto.getId());
        assertEquals(currencyId, dto.getCurrencyId());
    }

    @Test
    public void convertToDomain() {
        AccountDto dto = new AccountDto();
        dto.setBalance(balance);
        dto.setId(id);
        dto.setCurrencyId(currencyId);

        Account domain = converter.convertToDomain(dto);

        assertEquals(balance, domain.getBalance());
        assertEquals(id, domain.getId());
        assertEquals(currencyId, domain.getCurrencyId());
    }
}