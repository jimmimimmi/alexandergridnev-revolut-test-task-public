package com.revolut.rest.api.account.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.account.dto.AccountDto;
import com.revolut.rest.api.account.repository.AccountRepository;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.currency.domain.Currency;
import com.revolut.rest.api.currency.repository.CurrencyRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

public class AccountService {
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private CurrencyRepository currencyRepository;
    @Inject
    private AccountBalanceValidator accountBalanceValidator;
    @Inject
    private AccountConverter accountConverter;

    public ExecutionResult create(AccountDto accountDto) {
        BigDecimal balance = accountDto.getBalance();
        int currencyId = accountDto.getCurrencyId();
        int id = accountDto.getId();

        if (!accountBalanceValidator.isValidBalance(balance)) {
            return ExecutionResult.of(BAD_REQUEST,
                    "Balance must be between " +
                            accountBalanceValidator.MIN_VALUE_EXCLUSIVE + " and " +
                            accountBalanceValidator.MAX_VALUE_EXCLUSIVE,
                    balance);
        }

        Currency currency = currencyRepository.getById(currencyId);

        if (currency == null) {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + currencyId, currencyId);
        }

        Account accountToBeStored = accountConverter.convertToDomain(accountDto);
        Account previouslyExistedAccount = accountRepository.add(accountToBeStored);
        if (previouslyExistedAccount == null) {
            return ExecutionResult.SUCCESS;
        } else {
            return ExecutionResult.of(BAD_REQUEST, "Account already exists. ID: " + id, id);
        }
    }

    public ExecutionResult update(AccountDto accountDto) {
        BigDecimal balance = accountDto.getBalance();
        int currencyId = accountDto.getCurrencyId();
        int id = accountDto.getId();

        if (!accountBalanceValidator.isValidBalance(balance)) {
            return ExecutionResult.of(BAD_REQUEST,
                    "Balance must be between " +
                            accountBalanceValidator.MIN_VALUE_EXCLUSIVE + " and " +
                            accountBalanceValidator.MAX_VALUE_EXCLUSIVE,
                    balance);
        }
        Account accountToBeUpdated = accountRepository.get(id);
        if (accountToBeUpdated == null) {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + id, id);
        }

        if (accountToBeUpdated.getCurrencyId() != currencyId) {
            return ExecutionResult.of(BAD_REQUEST, "It is forbidden to update account's currency: " + currencyId, currencyId);
        }

        synchronized (accountToBeUpdated) {
            accountToBeUpdated.setBalance(balance);
        }
        return ExecutionResult.SUCCESS;
    }

    public ExecutionResult delete(int id) {
        Account removedAccount = accountRepository.delete(id);
        if (removedAccount != null) {
            return ExecutionResult.SUCCESS;
        } else {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + id, id);
        }
    }

    public AccountDto get(int id) {
        Account account = accountRepository.get(id);
        if (account == null) {
            return null;
        } else {
            return accountConverter.convertToDTO(account);
        }
    }

    public List<AccountDto> getAll() {
        Collection<Account> accounts = accountRepository.getAll();
        
        return accounts.stream().map(accountConverter::convertToDTO).collect(Collectors.toList());
    }

    AccountService setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        return this;
    }

    AccountService setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
        return this;
    }

    AccountService setAccountBalanceValidator(AccountBalanceValidator accountBalanceValidator) {
        this.accountBalanceValidator = accountBalanceValidator;
        return this;
    }

    AccountService setAccountConverter(AccountConverter accountConverter) {
        this.accountConverter = accountConverter;
        return this;
    }
}
