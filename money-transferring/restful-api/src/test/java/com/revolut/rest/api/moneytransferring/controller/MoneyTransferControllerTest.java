package com.revolut.rest.api.moneytransferring.controller;

import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.moneytransferring.dto.MoneyTransferDto;
import com.revolut.rest.api.moneytransferring.services.MoneyTransferService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MoneyTransferControllerTest {

    @Mock
    private MoneyTransferService moneyTransferService;
    @Mock
    private ClientExceptionFactory clientExceptionFactory;
    @Mock
    private MoneyTransferDto moneyTransferDto;

    private MoneyTransferController moneyTransferController;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        moneyTransferController = new MoneyTransferController();
        moneyTransferController.setClientExceptionFactory(clientExceptionFactory);
        moneyTransferController.setMoneyTransferService(moneyTransferService);
    }

    @Test
    public void transferMoney() {
        when(moneyTransferService.executeMoneyTransfer(moneyTransferDto)).thenReturn(ExecutionResult.SUCCESS);

        Response response = moneyTransferController.executeTransfer(moneyTransferDto);

        assertSame(ExecutionResult.SUCCESS, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException transferMoneyExceptionThrown = ExpectedException.none();

    @Test
    public void transferMoneyBadRequest() {
        ExecutionResult executionResult = ExecutionResult.of(Response.Status.BAD_REQUEST, "", "");
        when(moneyTransferService.executeMoneyTransfer(moneyTransferDto)).thenReturn(executionResult);
        when(clientExceptionFactory.create(executionResult)).thenReturn(new ClientErrorException(Response.Status.BAD_REQUEST));

        transferMoneyExceptionThrown.expect(ClientErrorException.class);
        moneyTransferController.executeTransfer(moneyTransferDto);
    }
}