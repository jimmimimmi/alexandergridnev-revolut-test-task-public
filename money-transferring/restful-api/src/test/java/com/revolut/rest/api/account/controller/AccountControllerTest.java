package com.revolut.rest.api.account.controller;

import com.revolut.rest.api.account.dto.AccountDto;
import com.revolut.rest.api.account.services.AccountService;
import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.common.ExecutionResult;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AccountControllerTest {

    @Mock
    private AccountService accountService;
    @Mock
    private ClientExceptionFactory clientExceptionFactory;
    @Mock
    private AccountDto accountDto;

    private AccountController accountController;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        accountController = new AccountController();
        accountController.setAccountService(accountService);
        accountController.setClientExceptionFactory(clientExceptionFactory);
    }

    @Test
    public void getAllAccounts() {
        ArrayList<AccountDto> allAccountsExpected = new ArrayList<>();
        when(accountService.getAll()).thenReturn(allAccountsExpected);

        List<AccountDto> allAccountsActual = accountController.getAllAccounts();

        assertSame(allAccountsExpected, allAccountsActual);
    }

    @Test
    public void getAccountById() {
        int id = 12345;
        when(accountService.get(id)).thenReturn(accountDto);

        Response response = accountController.getAccount(id);

        assertSame(accountDto, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException getAccountByIdExceptionThrown = ExpectedException.none();

    @Test
    public void getAccountByIdBadRequest() {
        int id = 12345;
        when(accountService.get(id)).thenReturn(null);
        when(clientExceptionFactory.create(any())).thenReturn(new ClientErrorException(Response.Status.NOT_FOUND));

        getAccountByIdExceptionThrown.expect(ClientErrorException.class);
        accountController.getAccount(id);
    }

    @Test
    public void createAccount() {
        when(accountService.create(accountDto)).thenReturn(ExecutionResult.SUCCESS);

        Response response = accountController.createAccount(accountDto);

        assertSame(ExecutionResult.SUCCESS, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException createAccountExceptionThrown = ExpectedException.none();

    @Test
    public void createAccountBadRequest() {
        ExecutionResult executionResult = ExecutionResult.of(Response.Status.BAD_REQUEST, "", "");
        when(accountService.create(accountDto)).thenReturn(executionResult);
        when(clientExceptionFactory.create(executionResult)).thenReturn(new ClientErrorException(Response.Status.BAD_REQUEST));

        getAccountByIdExceptionThrown.expect(ClientErrorException.class);
        accountController.createAccount(accountDto);
    }

    @Test
    public void updateAccount() {
        when(accountService.update(accountDto)).thenReturn(ExecutionResult.SUCCESS);

        Response response = accountController.updateAccount(accountDto);

        assertSame(ExecutionResult.SUCCESS, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException updateAccountExceptionThrown = ExpectedException.none();

    @Test
    public void updateAccountBadRequest() {
        ExecutionResult executionResult = ExecutionResult.of(Response.Status.BAD_REQUEST, "", "");
        when(accountService.update(accountDto)).thenReturn(executionResult);
        when(clientExceptionFactory.create(executionResult)).thenReturn(new ClientErrorException(Response.Status.BAD_REQUEST));

        getAccountByIdExceptionThrown.expect(ClientErrorException.class);
        accountController.updateAccount(accountDto);
    }

    @Test
    public void deleteAccount() {
        int id = 12345;
        when(accountService.delete(id)).thenReturn(ExecutionResult.SUCCESS);

        Response response = accountController.deleteAccount(id);

        assertSame(ExecutionResult.SUCCESS, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException deleteAccountExceptionThrown = ExpectedException.none();

    @Test
    public void deleteAccountBadRequest() {
        int id = 12345;
        ExecutionResult executionResult = ExecutionResult.of(Response.Status.BAD_REQUEST, "", "");
        when(accountService.delete(id)).thenReturn(executionResult);
        when(clientExceptionFactory.create(executionResult)).thenReturn(new ClientErrorException(Response.Status.BAD_REQUEST));

        getAccountByIdExceptionThrown.expect(ClientErrorException.class);
        accountController.deleteAccount(id);
    }
}