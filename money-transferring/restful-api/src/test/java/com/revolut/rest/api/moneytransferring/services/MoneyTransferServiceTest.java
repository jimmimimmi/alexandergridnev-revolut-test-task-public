package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.account.repository.AccountRepository;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.moneytransferring.dto.MoneyTransferDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.matchers.StringContains;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MoneyTransferServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private MoneyTransferAmountValidator moneyTransferAmountValidator;
    @Mock
    private MoneyTransferCrossCurrencyRateProvider moneyTransferCrossCurrencyRateProvider;
    @Mock
    private MoneyTransferExecutor moneyTransferExecutor;
    @Mock
    private Account fromAccount;
    @Mock
    private Account toAccount;
    @Mock
    private MoneyTransferDto moneyTransferDto;

    private int fromAccountId;
    private int toAccountId;
    private BigDecimal amount;
    private BigDecimal crossRate;

    private MoneyTransferService moneyTransferService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        moneyTransferService = new MoneyTransferService();
        moneyTransferService.setAccountRepository(accountRepository);
        moneyTransferService.setMoneyTransferAmountValidator(moneyTransferAmountValidator);
        moneyTransferService.setMoneyTransferCrossCurrencyRateProvider(moneyTransferCrossCurrencyRateProvider);
        moneyTransferService.setMoneyTransferExecutor(moneyTransferExecutor);

        fromAccountId = 12345;
        toAccountId = 6789;
        amount = BigDecimal.valueOf(45612);
        crossRate = BigDecimal.valueOf(12.2);

        when(moneyTransferDto.getFromAccountId()).thenReturn(fromAccountId);
        when(moneyTransferDto.getToAccountId()).thenReturn(toAccountId);
        when(moneyTransferDto.getAmount()).thenReturn(amount);

        when(accountRepository.get(fromAccountId)).thenReturn(fromAccount);
        when(accountRepository.get(toAccountId)).thenReturn(toAccount);

        when(moneyTransferCrossCurrencyRateProvider.getRate(fromAccount, toAccount)).thenReturn(crossRate);
        when(moneyTransferAmountValidator.isValidAmount(amount, fromAccount)).thenReturn(true);
    }


    @Test
    public void failToTransferFromAccountToItself() {
        when(moneyTransferDto.getFromAccountId()).thenReturn(fromAccountId);
        when(moneyTransferDto.getToAccountId()).thenReturn(fromAccountId);

        ExecutionResult executionResult = moneyTransferService.executeMoneyTransfer(moneyTransferDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("It is forbidden to transfer money to yourself"));
    }

    @Test
    public void failToTransferFromNonExistentAccount() {
        when(accountRepository.get(fromAccountId)).thenReturn(null);

        ExecutionResult executionResult = moneyTransferService.executeMoneyTransfer(moneyTransferDto);

        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with ID: " + fromAccountId));
    }

    @Test
    public void failToTransferToNonExistentAccount() {
        when(accountRepository.get(toAccountId)).thenReturn(null);

        ExecutionResult executionResult = moneyTransferService.executeMoneyTransfer(moneyTransferDto);

        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with ID: " + toAccountId));
    }

    @Test
    public void failToTransferIfThereIsNoCrossRate() {
        when(moneyTransferCrossCurrencyRateProvider.getRate(fromAccount, toAccount)).thenReturn(null);

        ExecutionResult executionResult = moneyTransferService.executeMoneyTransfer(moneyTransferDto);

        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No crossRate was found"));
    }

    @Test
    public void failToTransferNotValidAmountOfMoney() {
        when(moneyTransferAmountValidator.isValidAmount(amount, fromAccount)).thenReturn(false);

        ExecutionResult executionResult = moneyTransferService.executeMoneyTransfer(moneyTransferDto);

        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("Transfer amount has to be positive and do not exceed account's balance"));
    }

    @Test
    public void successfulTransfer() {
        ExecutionResult executionResult = moneyTransferService.executeMoneyTransfer(moneyTransferDto);

        verify(moneyTransferExecutor).executeTransfer(fromAccount, toAccount, amount, crossRate);
        assertSame(ExecutionResult.SUCCESS, executionResult);
    }

}