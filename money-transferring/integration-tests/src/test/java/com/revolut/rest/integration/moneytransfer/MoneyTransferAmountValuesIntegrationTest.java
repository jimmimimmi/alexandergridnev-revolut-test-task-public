package com.revolut.rest.integration.moneytransfer;

import com.revolut.rest.integration.BaseIntegrationTest;
import com.revolut.rest.integration.testutil.AccountClientUtil;
import com.revolut.rest.integration.testutil.MoneyTransferClientUtil;
import com.revolut.rest.integration.testutil.Status;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class MoneyTransferAmountValuesIntegrationTest extends BaseIntegrationTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0.0, 10.0, Status.BAD_REQUEST},
                {-1.0, 10.0, Status.BAD_REQUEST},
                {0.01, 10.0, Status.OK},
                {10.01, 10.0, Status.BAD_REQUEST},
                {1_999_999_999.99, 1_999_999_999.99, Status.OK},
                {2_000_000_000, 1_999_999_999.99, Status.BAD_REQUEST}
        });
    }

    private double initialBalance;
    private double amount;
    private Status status;

    public MoneyTransferAmountValuesIntegrationTest(double amount, double initialBalance, Status status) {
        this.initialBalance = initialBalance;
        this.amount = amount;
        this.status = status;
    }

    @Test
    public void makeTransfer() {
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, BigDecimal.valueOf(initialBalance));
        AccountClientUtil.createAccountAndVerifySuccess(2, 1, BigDecimal.TEN);

        MoneyTransferClientUtil.moneyTransferResponse(1, 2, BigDecimal.valueOf(amount))
                .statusCode(status.getCode());
    }


    @After
    public void tearDown() throws Exception {
        AccountClientUtil.removeAllAccounts();
    }

}
