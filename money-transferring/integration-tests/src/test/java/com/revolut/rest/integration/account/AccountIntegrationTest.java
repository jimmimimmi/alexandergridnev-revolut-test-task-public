package com.revolut.rest.integration.account;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.revolut.rest.integration.BaseIntegrationTest;
import com.revolut.rest.integration.domain.Account;
import com.revolut.rest.integration.testutil.RestfulApiEndPoints;
import com.revolut.rest.integration.testutil.AccountClientUtil;
import com.revolut.rest.integration.testutil.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.Assert.assertEquals;
import static org.junit.internal.matchers.StringContains.containsString;

public class AccountIntegrationTest extends BaseIntegrationTest {

    private int id;
    private int currencyId;
    private int nonExistentCurrencyId;
    private BigDecimal balance;

    @Before
    public void setUp() {
        id = 1;
        currencyId = 2;
        nonExistentCurrencyId = 10;
        balance = BigDecimal.TEN;
        RestAssured.basePath = RestfulApiEndPoints.ACCOUNT;
    }

    @After
    public void tearDown() throws Exception {
        AccountClientUtil.removeAllAccounts();
    }

    @Test
    public void accountIsNotFound() {
        AccountClientUtil.getAccountResponse(id).statusCode(Status.NOT_FOUND.getCode());
    }

    @Test
    public void createAccount() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);

        AccountClientUtil.getAccountAndAssert(id, currencyId, balance);
    }

    @Test
    public void createAccountsAndGetThemAll() throws IOException {
        int id1 = id;
        int id2 = id + 1;

        int currencyId1 = currencyId;
        int currencyId2 = currencyId + 1;

        BigDecimal balance1 = BigDecimal.valueOf(100);
        BigDecimal balance2 = BigDecimal.valueOf(Status.OK.getCode());

        AccountClientUtil.createAccountAndAssert(id1, currencyId1, balance1);
        AccountClientUtil.createAccountAndAssert(id2, currencyId2, balance2);

        List<Account> allAccounts = AccountClientUtil.getAllAccounts();
        assertEquals(2, allAccounts.size());

        Account account1 = allAccounts.stream().filter(account -> account.getId() == id1).findFirst().get();
        Account account2 = allAccounts.stream().filter(account -> account.getId() == id2).findFirst().get();

        assertEquals(currencyId1, account1.getCurrencyId());
        assertEquals(balance1, account1.getBalance());

        assertEquals(currencyId2, account2.getCurrencyId());
        assertEquals(balance2, account2.getBalance());
    }

    @Test
    public void canNotCreateAccountWithNonExistentCurrency() {
        AccountClientUtil.createAccountAndGetResponse(id, nonExistentCurrencyId, BigDecimal.TEN)
                .statusCode(Status.BAD_REQUEST.getCode())
                .contentType(ContentType.JSON)
                .body(containsString("No entity was found with ID: " + nonExistentCurrencyId));
    }

    @Test
    public void canNotCreateAccountWithOppupiedId() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);
        AccountClientUtil.createAccountAndGetResponse(id, currencyId + 1, BigDecimal.TEN)
                .statusCode(Status.BAD_REQUEST.getCode())
                .contentType(ContentType.JSON)
                .body(containsString("Account already exists. ID: " + id));
    }

    @Test
    public void deleteAccount() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);
        AccountClientUtil.deleteAccountAndAssert(id);
    }

    @Test
    public void canNotDeleteNonExistentAccount() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);
        AccountClientUtil.deleteAccountResponse(id + 1)
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("No entity was found with ID: " + (id + 1)));
    }

    @Test
    public void updateAccountBalance() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);
        AccountClientUtil.updateAccountAndAssert(id, currencyId, BigDecimal.ONE);
    }

    @Test
    public void canNotUpdateAccountCurrency() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);

        AccountClientUtil.updateAccountResponse(id, currencyId + 1, BigDecimal.TEN)
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("It is forbidden to update account's currency"));

        AccountClientUtil.getAccountAndAssert(id, currencyId, BigDecimal.TEN);
    }

    @Test
    public void canNotUpdateNonExistentAccount() throws IOException {
        AccountClientUtil.createAccountAndAssert(id, currencyId, BigDecimal.TEN);

        AccountClientUtil.updateAccountResponse(id + 1, currencyId, BigDecimal.TEN)
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("No entity was found with ID: " + (id + 1)));
    }


}
