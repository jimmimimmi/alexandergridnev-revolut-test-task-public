package com.revolut.rest.api.common;

import javax.ws.rs.core.Response;

public class ExecutionResult {
    public static final ExecutionResult SUCCESS = new ExecutionResult();

    private final Response.Status status;
    private final String message;
    private final String invalidValue;

    private ExecutionResult() {
        status = Response.Status.OK;
        message = null;
        invalidValue = null;
    }

    private ExecutionResult(Response.Status status, String message, String invalidValue) {
        this.status = status;
        this.message = message;
        this.invalidValue = invalidValue;
    }

    public static ExecutionResult of(Response.Status status, String message, Object invalidValue) {
        return new ExecutionResult(status, message, invalidValue.toString());
    }

    public Response.Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getInvalidValue() {
        return invalidValue;
    }
}
