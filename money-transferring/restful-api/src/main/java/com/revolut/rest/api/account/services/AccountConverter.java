package com.revolut.rest.api.account.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.account.dto.AccountDto;


public class AccountConverter {
    public AccountDto convertToDTO(Account domain) {
        AccountDto dto = new AccountDto();

        dto.setCurrencyId(domain.getCurrencyId());
        dto.setBalance(domain.getBalance());
        dto.setId(domain.getId());

        return dto;
    }

    public Account convertToDomain(AccountDto dto) {
        return new Account(dto.getId(), dto.getCurrencyId(), dto.getBalance());
    }
}
