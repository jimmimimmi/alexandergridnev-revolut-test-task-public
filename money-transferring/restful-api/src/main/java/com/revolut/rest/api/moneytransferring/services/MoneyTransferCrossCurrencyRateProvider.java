package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;
import com.revolut.rest.api.currency.repository.CrossCurrencyRateRepository;

import javax.inject.Inject;
import java.math.BigDecimal;

public class MoneyTransferCrossCurrencyRateProvider {
    @Inject
    private CrossCurrencyRateRepository crossCurrencyRateRepository;

    public BigDecimal getRate(Account fromAccount, Account toAccount) {
        if (fromAccount.getCurrencyId() == toAccount.getCurrencyId()) {
            return BigDecimal.ONE;
        }

        CrossCurrencyRate crossCurrencyRate = crossCurrencyRateRepository.get(fromAccount.getCurrencyId(), toAccount.getCurrencyId());
        if (crossCurrencyRate == null) {
            return null;
        } else {
            return crossCurrencyRate.getRate();
        }
    }

    MoneyTransferCrossCurrencyRateProvider setCrossCurrencyRateRepository(CrossCurrencyRateRepository crossCurrencyRateRepository) {
        this.crossCurrencyRateRepository = crossCurrencyRateRepository;
        return this;
    }
}
