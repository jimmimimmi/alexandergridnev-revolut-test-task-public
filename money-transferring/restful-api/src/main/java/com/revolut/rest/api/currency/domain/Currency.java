package com.revolut.rest.api.currency.domain;

public class Currency {
    private final int id;

    public Currency(int id) {
        this.id = id;
    }

    public static Currency of(int id) {
        return new Currency(id);
    }

    public int getId() {
        return id;
    }
}
