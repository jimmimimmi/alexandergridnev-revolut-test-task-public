package com.revolut.rest.api.account.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.account.dto.AccountDto;
import com.revolut.rest.api.account.repository.AccountRepository;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.currency.domain.Currency;
import com.revolut.rest.api.currency.repository.CurrencyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.matchers.StringContains;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private CurrencyRepository currencyRepository;
    @Mock
    private AccountBalanceValidator accountBalanceValidator;
    @Mock
    private AccountConverter accountConverter;

    @Mock
    private AccountDto accountDto;
    @Mock
    private AccountDto accountDto2;
    @Mock
    private Account account;
    @Mock
    private Account account2;

    private AccountService accountService;

    private int id;
    private int currencyId;
    private BigDecimal balance;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        id = 12345;
        currencyId = 456789;
        balance = BigDecimal.valueOf(78945.45);

        accountService = new AccountService();
        accountService.setAccountBalanceValidator(accountBalanceValidator);
        accountService.setAccountConverter(accountConverter);
        accountService.setAccountRepository(accountRepository);
        accountService.setCurrencyRepository(currencyRepository);

        when(accountRepository.getAll()).thenReturn(Arrays.asList(account, account2));
        when(accountConverter.convertToDTO(account)).thenReturn(accountDto);
        when(accountConverter.convertToDTO(account2)).thenReturn(accountDto2);
        when(accountConverter.convertToDomain(accountDto)).thenReturn(account);
        when(accountConverter.convertToDomain(accountDto2)).thenReturn(account2);

        when(accountDto.getId()).thenReturn(id);
        when(accountDto.getCurrencyId()).thenReturn(currencyId);
        when(accountDto.getBalance()).thenReturn(balance);

        when(accountBalanceValidator.isValidBalance(balance)).thenReturn(true);
        when(currencyRepository.getById(currencyId)).thenReturn(new Currency(currencyId));
    }

    @Test
    public void getAll() {
        List<AccountDto> all = accountService.getAll();

        assertEquals(2, all.size());
        assertSame(accountDto, all.get(0));
        assertSame(accountDto2, all.get(1));
    }

    @Test
    public void getAccountById() {
        when(accountRepository.get(id)).thenReturn(account);

        AccountDto accountDto = accountService.get(id);

        assertSame(this.accountDto, accountDto);
    }

    @Test
    public void accountByIdIsNull() {
        when(accountRepository.get(id)).thenReturn(null);

        AccountDto accountDto = accountService.get(id);

        assertNull(accountDto);
    }

    @Test
    public void failToCreateAccountWithBadBalance() {
        when(accountBalanceValidator.isValidBalance(balance)).thenReturn(false);

        ExecutionResult executionResult = accountService.create(accountDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("Balance must be between 0"));
    }

    @Test
    public void failToCreateAccountWithNonExistentCurrency() {
        when(currencyRepository.getById(currencyId)).thenReturn(null);

        ExecutionResult executionResult = accountService.create(accountDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with ID: " + currencyId));
    }

    @Test
    public void failToCreateAccountWithOccupiedId() {
        when(accountRepository.add(account)).thenReturn(account2);

        ExecutionResult executionResult = accountService.create(accountDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("Account already exists. ID: " + id));
    }

    @Test
    public void createAccount() {
        ExecutionResult executionResult = accountService.create(accountDto);

        assertSame(ExecutionResult.SUCCESS, executionResult);
    }

    @Test
    public void failToUpdateAccountWithBadBalance() {
        when(accountBalanceValidator.isValidBalance(balance)).thenReturn(false);

        ExecutionResult executionResult = accountService.update(accountDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("Balance must be between 0"));
    }

    @Test
    public void failToUpdateNonExistentAccount() {
        when(accountRepository.get(id)).thenReturn(null);

        ExecutionResult executionResult = accountService.update(accountDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with ID: " + id));
    }


    @Test
    public void failToUpdateAccountWithNonExistentCurrency() {
        when(accountRepository.get(id)).thenReturn(account2);
        when(account2.getCurrencyId()).thenReturn(currencyId + 1);

        ExecutionResult executionResult = accountService.update(accountDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("It is forbidden to update account's currency: " + currencyId));
    }

    @Test
    public void updateAccount() {
        when(accountRepository.get(id)).thenReturn(account2);
        when(account2.getCurrencyId()).thenReturn(currencyId);
        ExecutionResult executionResult = accountService.update(accountDto);

        assertSame(ExecutionResult.SUCCESS, executionResult);
        verify(account2).setBalance(accountDto.getBalance());
    }

    @Test
    public void deleteAccount() {
        when(accountRepository.delete(id)).thenReturn(account);

        ExecutionResult executionResult = accountService.delete(id);

        assertSame(ExecutionResult.SUCCESS, executionResult);
    }

    @Test
    public void canNotDeleteNonExistentAccount() {
        when(accountRepository.delete(id)).thenReturn(null);

        ExecutionResult executionResult = accountService.delete(id);

        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with ID: " + id));
    }
}