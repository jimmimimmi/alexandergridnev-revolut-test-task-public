package com.revolut.rest.integration.moneytransfer;

import com.revolut.rest.integration.BaseIntegrationTest;
import com.revolut.rest.integration.testutil.AccountClientUtil;
import com.revolut.rest.integration.testutil.CrossCurrencyRateClienUtil;
import com.revolut.rest.integration.testutil.MoneyTransferClientUtil;
import com.revolut.rest.integration.testutil.Status;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.internal.matchers.StringContains.containsString;

public class MoneyTransferIntegrationTest extends BaseIntegrationTest {
    @After
    public void tearDown() throws Exception {
        AccountClientUtil.removeAllAccounts();
        CrossCurrencyRateClienUtil.deleteCrossCurrencyRateResponse(1, 2);
    }

    @Test
    public void transferBetweenAccountsWithTheSameCurrency() throws IOException {
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, BigDecimal.valueOf(100.0));
        AccountClientUtil.createAccountAndVerifySuccess(2, 1, BigDecimal.valueOf(200.0));

        MoneyTransferClientUtil.moneyTransferAndVerifySuccess(1, 2, BigDecimal.valueOf(60.5));

        AccountClientUtil.getAccountAndAssert(1, 1, BigDecimal.valueOf(39.5));
        AccountClientUtil.getAccountAndAssert(2, 1, BigDecimal.valueOf(260.5));
    }

    @Test
    public void canNotTransferToNonExistentAccount() throws IOException {
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, BigDecimal.valueOf(100.0));

        MoneyTransferClientUtil.moneyTransferResponse(1, 3, BigDecimal.valueOf(60.5))
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("No entity was found with ID: " + 3));

        AccountClientUtil.getAccountAndAssert(1, 1, BigDecimal.valueOf(100.0));
    }

    @Test
    public void canNotTransferFromNonExistentAccount() throws IOException {
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, BigDecimal.valueOf(100.0));

        MoneyTransferClientUtil.moneyTransferResponse(3, 1, BigDecimal.valueOf(60.5))
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("No entity was found with ID: " + 3));

        AccountClientUtil.getAccountAndAssert(1, 1, BigDecimal.valueOf(100.0));
    }

    @Test
    public void canNotTransferToYourself() throws IOException {
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, BigDecimal.valueOf(100.0));

        MoneyTransferClientUtil.moneyTransferResponse(1, 1, BigDecimal.valueOf(60.5))
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("It is forbidden to transfer money to yourself: " + 1));

        AccountClientUtil.getAccountAndAssert(1, 1, BigDecimal.valueOf(100.0));
    }

    @Test
    public void canNotTransferIfThereIsNoCrossCurrencyRate() throws IOException {
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, BigDecimal.valueOf(100.0));
        AccountClientUtil.createAccountAndVerifySuccess(2, 2, BigDecimal.valueOf(200.0));

        MoneyTransferClientUtil.moneyTransferResponse(1, 2, BigDecimal.valueOf(60.5))
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString("No crossRate was found"));

        AccountClientUtil.getAccountAndAssert(1, 1, BigDecimal.valueOf(100.0));
        AccountClientUtil.getAccountAndAssert(2, 2, BigDecimal.valueOf(200.0));
    }

    @Test
    public void transferIfThereIsCrossCurrencyRate() throws IOException {
        BigDecimal balance1 = BigDecimal.valueOf(100.0);
        BigDecimal balance2 = BigDecimal.valueOf(200.0);
        AccountClientUtil.createAccountAndVerifySuccess(1, 1, balance1);
        AccountClientUtil.createAccountAndVerifySuccess(2, 2, balance2);

        BigDecimal crossRate = BigDecimal.valueOf(1.5);
        CrossCurrencyRateClienUtil.createCrossCurrencyRateAndAssert(1, 2, crossRate);

        BigDecimal moneyTransferAmount = BigDecimal.valueOf(60);
        MoneyTransferClientUtil.moneyTransferAndVerifySuccess(1, 2, moneyTransferAmount);

        AccountClientUtil.getAccountAndAssert(1, 1, balance1.subtract(moneyTransferAmount));
        AccountClientUtil.getAccountAndAssert(2, 2, balance2.add(moneyTransferAmount.multiply(crossRate)));
    }

}
