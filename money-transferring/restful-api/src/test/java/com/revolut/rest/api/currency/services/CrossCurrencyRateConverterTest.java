package com.revolut.rest.api.currency.services;

import com.revolut.rest.api.currency.domain.CrossCurrency;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;
import com.revolut.rest.api.currency.dto.CrossCurrencyRateDto;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class CrossCurrencyRateConverterTest {
    private CrossCurrencyRateConverter converter;
    private int fromCurrencyId;
    private int toCurrencyId;
    private BigDecimal rate;


    @Before
    public void setUp() throws Exception {
        converter = new CrossCurrencyRateConverter();

        Random random = new Random();
        fromCurrencyId = random.nextInt();
        toCurrencyId = random.nextInt();
        rate = BigDecimal.valueOf(random.nextLong());
    }

    @Test
    public void convertToDTO() {
        CrossCurrencyRateDto dto = converter.convertToDTO(new CrossCurrencyRate(CrossCurrency.of(fromCurrencyId, toCurrencyId), rate));

        assertEquals(rate, dto.getRate());
        assertEquals(fromCurrencyId, dto.getFromCurrencyId());
        assertEquals(toCurrencyId, dto.getToCurrencyId());
    }

    @Test
    public void convertToDomain() {
        CrossCurrencyRateDto dto = new CrossCurrencyRateDto();
        dto.setRate(rate);
        dto.setFromCurrencyId(fromCurrencyId);
        dto.setToCurrencyId(toCurrencyId);

        CrossCurrencyRate domain = converter.convertToDomain(dto);

        assertEquals(rate, domain.getRate());
        assertEquals(fromCurrencyId, domain.getFromCurrencyId());
        assertEquals(toCurrencyId, domain.getToCurrencyId());
    }
}