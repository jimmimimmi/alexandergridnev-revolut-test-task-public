package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;

import java.math.BigDecimal;

public class MoneyTransferExecutor {
    public void executeTransfer(Account fromAccount, Account toAccount, BigDecimal amount, BigDecimal crossRate) {
        BigDecimal newBalanceForFromAccount = fromAccount.getBalance().subtract(amount);
        BigDecimal newBalanceForToAccount = toAccount.getBalance().add(amount.multiply(crossRate));

        fromAccount.setBalance(newBalanceForFromAccount);
        toAccount.setBalance(newBalanceForToAccount);
    }
}
