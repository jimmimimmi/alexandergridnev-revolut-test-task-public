package com.revolut.rest.integration.testutil;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import com.revolut.rest.integration.domain.MoneyTransfer;

import java.math.BigDecimal;

import static com.jayway.restassured.RestAssured.given;

public class MoneyTransferClientUtil {
    public static ValidatableResponse moneyTransferResponse(int fromAccountId, int toAccountId, BigDecimal amount) {
        RestAssured.basePath = RestfulApiEndPoints.TRANSFER;
        return given().contentType("application/json")
                .accept(ContentType.JSON)
                .body(MoneyTransfer.of(fromAccountId, toAccountId, amount))
                .when()
                .post()
                .then();
    }

    public static void moneyTransferAndVerifySuccess(int fromAccountId, int toAccountId, BigDecimal amount) {
        moneyTransferResponse(fromAccountId, toAccountId, amount).statusCode(Status.OK.getCode());
    }
}
