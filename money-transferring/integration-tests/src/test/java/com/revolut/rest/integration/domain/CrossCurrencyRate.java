package com.revolut.rest.integration.domain;

import java.math.BigDecimal;

public class CrossCurrencyRate {
    private int fromCurrencyId;
    private int toCurrencyId;
    private BigDecimal rate;

    public int getFromCurrencyId() {
        return fromCurrencyId;
    }

    public void setFromCurrencyId(int fromCurrencyId) {
        this.fromCurrencyId = fromCurrencyId;
    }

    public int getToCurrencyId() {
        return toCurrencyId;
    }

    public void setToCurrencyId(int toCurrencyId) {
        this.toCurrencyId = toCurrencyId;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public static CrossCurrencyRate of(int fromCurrencyId, int toCurrencyId, BigDecimal rate) {
        CrossCurrencyRate dto = new CrossCurrencyRate();

        dto.setFromCurrencyId(fromCurrencyId);
        dto.setToCurrencyId(toCurrencyId);
        dto.setRate(rate);

        return dto;
    }
}
