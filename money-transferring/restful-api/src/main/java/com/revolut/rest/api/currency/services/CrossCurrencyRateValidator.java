package com.revolut.rest.api.currency.services;

import java.math.BigDecimal;

public class CrossCurrencyRateValidator {
    public static final int MIN_VALUE_EXCLUSIVE = 0;
    public static final int MAX_VALUE_EXCLUSIVE = 2_000_000_000;

    public boolean isValidCrossRate(BigDecimal rate) {
        int comparingWithMinValue = rate.compareTo(BigDecimal.valueOf(MIN_VALUE_EXCLUSIVE));
        int comparingWithMaxValue = rate.compareTo(BigDecimal.valueOf(MAX_VALUE_EXCLUSIVE));
        return comparingWithMinValue > 0 && comparingWithMaxValue < 0;
    }
}
