package com.revolut.rest.api.moneytransferring.controller;

import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.moneytransferring.dto.MoneyTransferDto;
import com.revolut.rest.api.moneytransferring.services.MoneyTransferService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transfer")
public class MoneyTransferController {
    @Inject
    private MoneyTransferService moneyTransferService;
    @Inject
    private ClientExceptionFactory clientExceptionFactory;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response executeTransfer(MoneyTransferDto moneyTransferDto) {
        ExecutionResult result = moneyTransferService.executeMoneyTransfer(moneyTransferDto);

        if (result == ExecutionResult.SUCCESS) {
            return Response.ok(result).build();
        } else {
            throw clientExceptionFactory.create(result);
        }
    }

    MoneyTransferController setMoneyTransferService(MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
        return this;
    }

    MoneyTransferController setClientExceptionFactory(ClientExceptionFactory clientExceptionFactory) {
        this.clientExceptionFactory = clientExceptionFactory;
        return this;
    }
}
