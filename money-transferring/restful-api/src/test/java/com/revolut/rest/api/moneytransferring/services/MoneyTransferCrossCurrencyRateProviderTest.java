package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;
import com.revolut.rest.api.currency.repository.CrossCurrencyRateRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MoneyTransferCrossCurrencyRateProviderTest {

    @Mock
    private CrossCurrencyRateRepository crossCurrencyRateRepository;
    @Mock
    private Account fromAccount;
    @Mock
    private Account toAccount;
    @Mock
    private CrossCurrencyRate crossCurrencyRate;

    private MoneyTransferCrossCurrencyRateProvider crossCurrencyRateProvider;
    private int currencyId;
    private BigDecimal expectedRate;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        crossCurrencyRateProvider = new MoneyTransferCrossCurrencyRateProvider();
        crossCurrencyRateProvider.setCrossCurrencyRateRepository(crossCurrencyRateRepository);

        currencyId = 12345;
        when(fromAccount.getCurrencyId()).thenReturn(currencyId);
        when(toAccount.getCurrencyId()).thenReturn(currencyId + 1);
        when(crossCurrencyRateRepository.get(currencyId, currencyId + 1)).thenReturn(crossCurrencyRate);
        expectedRate = BigDecimal.TEN;
        when(crossCurrencyRate.getRate()).thenReturn(expectedRate);
    }

    @Test
    public void oneForTheSameCurrencyId() {
        when(toAccount.getCurrencyId()).thenReturn(currencyId);
        BigDecimal rate = crossCurrencyRateProvider.getRate(fromAccount, toAccount);
        assertEquals(BigDecimal.ONE, rate);
    }

    @Test
    public void nullIfThereIsNoCurrencyRateInRepository() {
        when(crossCurrencyRateRepository.get(currencyId, currencyId + 1)).thenReturn(null);
        BigDecimal rate = crossCurrencyRateProvider.getRate(fromAccount, toAccount);
        assertNull(rate);
    }

    @Test
    public void getRate() {
        BigDecimal rate = crossCurrencyRateProvider.getRate(fromAccount, toAccount);
        assertEquals(expectedRate, rate);
    }
}