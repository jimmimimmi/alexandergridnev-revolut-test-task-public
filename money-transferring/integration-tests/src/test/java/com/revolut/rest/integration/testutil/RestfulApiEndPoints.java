package com.revolut.rest.integration.testutil;

public class RestfulApiEndPoints {
    public static final String CROSS_RATES = "/agridnev-revolut/cross-rates/";
    public static final String ACCOUNT = "/agridnev-revolut/account/";
    public static final String TRANSFER = "/agridnev-revolut/transfer/";
}
