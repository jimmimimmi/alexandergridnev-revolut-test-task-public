package com.revolut.rest.api.currency.controller;

import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.currency.dto.CrossCurrencyRateDto;
import com.revolut.rest.api.currency.services.CrossCurrencyRateService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CrossCurrencyRateControllerTest {
    @Mock
    private CrossCurrencyRateService crossCurrencyRateService;
    @Mock
    private ClientExceptionFactory clientExceptionFactory;
    @Mock
    private CrossCurrencyRateDto crossCurrencyRateDto;

    private CrossCurrencyRateController crossCurrencyRateController;
    private int toCurrencyId;
    private int fromCurrencyId;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        crossCurrencyRateController = new CrossCurrencyRateController();
        crossCurrencyRateController.setCrossCurrencyRateService(crossCurrencyRateService);
        crossCurrencyRateController.setClientExceptionFactory(clientExceptionFactory);

        fromCurrencyId = 12345;
        toCurrencyId = 45678;
    }

    @Test
    public void getCrossCurrencyRateById() {

        when(crossCurrencyRateService.get(fromCurrencyId, toCurrencyId)).thenReturn(crossCurrencyRateDto);

        Response response = crossCurrencyRateController.getCrossCurrencyRate(fromCurrencyId, toCurrencyId);

        assertSame(crossCurrencyRateDto, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException getCrossCurrencyRateByIdExceptionThrown = ExpectedException.none();

    @Test
    public void getCrossCurrencyRateByIdBadRequest() {
        when(crossCurrencyRateService.get(fromCurrencyId, toCurrencyId)).thenReturn(null);
        when(clientExceptionFactory.create(any())).thenReturn(new ClientErrorException(Response.Status.NOT_FOUND));

        getCrossCurrencyRateByIdExceptionThrown.expect(ClientErrorException.class);
        crossCurrencyRateController.getCrossCurrencyRate(fromCurrencyId, toCurrencyId);
    }

    @Test
    public void createCrossCurrencyRate() {
        when(crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto)).thenReturn(ExecutionResult.SUCCESS);

        Response response = crossCurrencyRateController.createOrUpdateCrossCurrencyRate(crossCurrencyRateDto);

        assertSame(ExecutionResult.SUCCESS, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException createCrossCurrencyRateExceptionThrown = ExpectedException.none();

    @Test
    public void createCrossCurrencyRateBadRequest() {
        ExecutionResult executionResult = ExecutionResult.of(Response.Status.BAD_REQUEST, "", "");
        when(crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto)).thenReturn(executionResult);
        when(clientExceptionFactory.create(executionResult)).thenReturn(new ClientErrorException(Response.Status.BAD_REQUEST));

        getCrossCurrencyRateByIdExceptionThrown.expect(ClientErrorException.class);
        crossCurrencyRateController.createOrUpdateCrossCurrencyRate(crossCurrencyRateDto);
    }


    @Test
    public void deleteCrossCurrencyRate() {
        when(crossCurrencyRateService.delete(fromCurrencyId, toCurrencyId)).thenReturn(ExecutionResult.SUCCESS);

        Response response = crossCurrencyRateController.deleteCrossCurrencyRate(fromCurrencyId, toCurrencyId);

        assertSame(ExecutionResult.SUCCESS, response.getEntity());
        assertEquals(200, response.getStatus());
    }

    @Rule
    public ExpectedException deleteCrossCurrencyRateExceptionThrown = ExpectedException.none();

    @Test
    public void deleteCrossCurrencyRateBadRequest() {
        ExecutionResult executionResult = ExecutionResult.of(Response.Status.BAD_REQUEST, "", "");
        when(crossCurrencyRateService.delete(fromCurrencyId, toCurrencyId)).thenReturn(executionResult);
        when(clientExceptionFactory.create(executionResult)).thenReturn(new ClientErrorException(Response.Status.BAD_REQUEST));

        getCrossCurrencyRateByIdExceptionThrown.expect(ClientErrorException.class);
        crossCurrencyRateController.deleteCrossCurrencyRate(fromCurrencyId, toCurrencyId);
    }
}