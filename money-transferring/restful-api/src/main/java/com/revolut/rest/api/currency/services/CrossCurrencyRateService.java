package com.revolut.rest.api.currency.services;

import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.currency.domain.CrossCurrency;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;
import com.revolut.rest.api.currency.domain.Currency;
import com.revolut.rest.api.currency.dto.CrossCurrencyRateDto;
import com.revolut.rest.api.currency.repository.CrossCurrencyRateRepository;
import com.revolut.rest.api.currency.repository.CurrencyRepository;

import javax.inject.Inject;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

public class CrossCurrencyRateService {

    @Inject
    private CrossCurrencyRateRepository crossCurrencyRateRepository;

    @Inject
    private CrossCurrencyRateConverter crossCurrencyRateConverter;

    @Inject
    private CurrencyRepository currencyRepository;

    @Inject
    private CrossCurrencyRateValidator crossCurrencyRateValidator;


    public CrossCurrencyRateDto get(int fromCurrencyId, int toCurrencyId) {
        CrossCurrencyRate domain = crossCurrencyRateRepository.get(fromCurrencyId, toCurrencyId);
        if (domain == null) {
            return null;
        }
        return crossCurrencyRateConverter.convertToDTO(domain);
    }

    public ExecutionResult createOrUpdate(CrossCurrencyRateDto crossCurrencyRateDto) {
        int fromCurrencyId = crossCurrencyRateDto.getFromCurrencyId();
        int toCurrencyId = crossCurrencyRateDto.getToCurrencyId();

        if (toCurrencyId == fromCurrencyId) {
            return ExecutionResult.of(BAD_REQUEST, "It is forbidden to create cross rate for the same currency", fromCurrencyId);
        }

        if (!crossCurrencyRateValidator.isValidCrossRate(crossCurrencyRateDto.getRate())) {
            return ExecutionResult.of(BAD_REQUEST,
                    "Cross rate must be between " +
                            crossCurrencyRateValidator.MIN_VALUE_EXCLUSIVE +
                            " and " +
                            crossCurrencyRateValidator.MAX_VALUE_EXCLUSIVE,
                    crossCurrencyRateDto.getRate());
        }

        Currency fromCurrency = currencyRepository.getById(fromCurrencyId);
        Currency toCurrency = currencyRepository.getById(toCurrencyId);

        if (fromCurrency == null) {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + fromCurrencyId, fromCurrencyId);
        }
        if (toCurrency == null) {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + toCurrencyId, toCurrencyId);
        }

        CrossCurrencyRate domain = crossCurrencyRateConverter.convertToDomain(crossCurrencyRateDto);
        crossCurrencyRateRepository.add(domain);

        return ExecutionResult.SUCCESS;
    }

    public ExecutionResult delete(int fromCurrencyId, int toCurrencyId) {
        CrossCurrencyRate removedCrossRate = crossCurrencyRateRepository.delete(CrossCurrency.of(fromCurrencyId, toCurrencyId));
        if (removedCrossRate != null) {
            return ExecutionResult.SUCCESS;
        } else {
            String invalidData = "[" + fromCurrencyId + ", " + toCurrencyId + "]";
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + invalidData, invalidData);
        }
    }

    void setCrossCurrencyRateRepository(CrossCurrencyRateRepository crossCurrencyRateRepository) {
        this.crossCurrencyRateRepository = crossCurrencyRateRepository;
    }

    void setCrossCurrencyRateConverter(CrossCurrencyRateConverter crossCurrencyRateConverter) {
        this.crossCurrencyRateConverter = crossCurrencyRateConverter;
    }

    void setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    void setCrossCurrencyRateValidator(CrossCurrencyRateValidator crossCurrencyRateValidator) {
        this.crossCurrencyRateValidator = crossCurrencyRateValidator;
    }
}
