package com.revolut.rest.api.common;

import org.glassfish.jersey.server.validation.ValidationError;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;


public class ClientExceptionFactory {
    public ClientErrorException create(ExecutionResult executionResult) {

        String message = executionResult.getMessage();
        Response response = Response.status(executionResult.getStatus())
                .entity(new ValidationError(message, null, null, executionResult.getInvalidValue()))
                .build();

        return new ClientErrorException(message, response);
    }
}
