package com.revolut.rest.api.account.services;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class AccountBalanceValidatorTest {

    private AccountBalanceValidator accountBalanceValidator;

    @Before
    public void setUp() throws Exception {
        accountBalanceValidator = new AccountBalanceValidator();
    }

    @Test
    public void negativeBalanceIsNotAllowed() {
        assertFalse(accountBalanceValidator.isValidBalance(BigDecimal.valueOf(-1)));
    }

    @Test
    public void zeroBalanceIsNotAllowed() {
        assertFalse(accountBalanceValidator.isValidBalance(BigDecimal.ZERO));
    }

    @Test
    public void BalanceExceededMaxIsNotAllowed() {
        assertFalse(accountBalanceValidator.isValidBalance(BigDecimal.valueOf(2_000_000_000)));
    }

    @Test
    public void positiveBalanceIsAllowed() {
        assertTrue(accountBalanceValidator.isValidBalance(BigDecimal.valueOf(0.01)));
        assertTrue(accountBalanceValidator.isValidBalance(BigDecimal.valueOf(1)));
        assertTrue(accountBalanceValidator.isValidBalance(BigDecimal.valueOf(1_999_999_999.99)));
    }
}