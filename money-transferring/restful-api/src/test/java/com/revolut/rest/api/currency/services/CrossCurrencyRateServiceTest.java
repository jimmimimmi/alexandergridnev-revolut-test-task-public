package com.revolut.rest.api.currency.services;

import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.currency.domain.CrossCurrency;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;
import com.revolut.rest.api.currency.domain.Currency;
import com.revolut.rest.api.currency.dto.CrossCurrencyRateDto;
import com.revolut.rest.api.currency.repository.CrossCurrencyRateRepository;
import com.revolut.rest.api.currency.repository.CurrencyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.matchers.StringContains;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CrossCurrencyRateServiceTest {

    @Mock
    private CrossCurrencyRateRepository crossCurrencyRateRepository;
    @Mock
    private CurrencyRepository currencyRepository;
    @Mock
    private CrossCurrencyRateValidator crossCurrencyRateValidator;
    @Mock
    private CrossCurrencyRateConverter crossCurrencyRateConverter;
    @Mock
    private CrossCurrencyRateDto crossCurrencyRateDto;
    @Mock
    private CrossCurrencyRate crossCurrencyRate;

    private CrossCurrencyRateService crossCurrencyRateService;

    private int fromCurrencyId;
    private int toCurrencyId;
    private BigDecimal rate;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        fromCurrencyId = 12345;
        toCurrencyId = 456789;
        rate = BigDecimal.valueOf(1.45);

        crossCurrencyRateService = new CrossCurrencyRateService();
        crossCurrencyRateService.setCrossCurrencyRateValidator(crossCurrencyRateValidator);
        crossCurrencyRateService.setCrossCurrencyRateConverter(crossCurrencyRateConverter);
        crossCurrencyRateService.setCrossCurrencyRateRepository(crossCurrencyRateRepository);
        crossCurrencyRateService.setCurrencyRepository(currencyRepository);

        when(crossCurrencyRateConverter.convertToDTO(crossCurrencyRate)).thenReturn(crossCurrencyRateDto);
        when(crossCurrencyRateConverter.convertToDomain(crossCurrencyRateDto)).thenReturn(crossCurrencyRate);

        when(crossCurrencyRateDto.getFromCurrencyId()).thenReturn(fromCurrencyId);
        when(crossCurrencyRateDto.getToCurrencyId()).thenReturn(toCurrencyId);
        when(crossCurrencyRateDto.getRate()).thenReturn(rate);

        when(crossCurrencyRateValidator.isValidCrossRate(rate)).thenReturn(true);
        when(currencyRepository.getById(toCurrencyId)).thenReturn(new Currency(toCurrencyId));
        when(currencyRepository.getById(fromCurrencyId)).thenReturn(new Currency(fromCurrencyId));
    }

    @Test
    public void getCrossCurrencyRateById() {
        when(crossCurrencyRateRepository.get(fromCurrencyId, toCurrencyId)).thenReturn(crossCurrencyRate);

        CrossCurrencyRateDto crossCurrencyRateDto = crossCurrencyRateService.get(fromCurrencyId, toCurrencyId);

        assertSame(this.crossCurrencyRateDto, crossCurrencyRateDto);
    }

    @Test
    public void crossCurrencyRateByIdIsNull() {
        when(crossCurrencyRateRepository.get(fromCurrencyId, fromCurrencyId)).thenReturn(null);

        CrossCurrencyRateDto crossCurrencyRateDto = crossCurrencyRateService.get(fromCurrencyId, fromCurrencyId);

        assertNull(crossCurrencyRateDto);
    }

    @Test
    public void failToCreateCrossCurrencyRateWithBadBalance() {
        when(crossCurrencyRateValidator.isValidCrossRate(rate)).thenReturn(false);

        ExecutionResult executionResult = crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("Cross rate must be between 0"));
    }

    @Test
    public void failToCreateCrossCurrencyRateWithNonExistentToCurrency() {
        when(currencyRepository.getById(toCurrencyId)).thenReturn(null);

        ExecutionResult executionResult = crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with"));
    }

    @Test
    public void failToCreateCrossCurrencyRateWithNonExistentFromCurrency() {
        when(currencyRepository.getById(fromCurrencyId)).thenReturn(null);

        ExecutionResult executionResult = crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto);
        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with"));
    }


    @Test
    public void createCrossCurrencyRate() {
        ExecutionResult executionResult = crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto);

        assertSame(ExecutionResult.SUCCESS, executionResult);
    }

    @Test
    public void deleteCrossCurrencyRate() {
        when(crossCurrencyRateRepository.delete(CrossCurrency.of(fromCurrencyId, toCurrencyId))).thenReturn(crossCurrencyRate);

        ExecutionResult executionResult = crossCurrencyRateService.delete(fromCurrencyId, toCurrencyId);

        assertSame(ExecutionResult.SUCCESS, executionResult);
    }

    @Test
    public void canNotDeleteNonExistentCrossCurrencyRate() {
        when(crossCurrencyRateRepository.delete(CrossCurrency.of(fromCurrencyId, toCurrencyId))).thenReturn(null);

        ExecutionResult executionResult = crossCurrencyRateService.delete(fromCurrencyId, toCurrencyId);

        assertEquals(Response.Status.BAD_REQUEST, executionResult.getStatus());
        assertThat(executionResult.getMessage(), StringContains.containsString("No entity was found with"));
    }

}