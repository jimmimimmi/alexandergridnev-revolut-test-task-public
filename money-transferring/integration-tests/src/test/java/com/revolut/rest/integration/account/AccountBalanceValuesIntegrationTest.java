package com.revolut.rest.integration.account;

import com.revolut.rest.integration.BaseIntegrationTest;
import com.revolut.rest.integration.testutil.AccountClientUtil;
import com.revolut.rest.integration.testutil.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static com.jayway.restassured.RestAssured.given;


@RunWith(Parameterized.class)
public class AccountBalanceValuesIntegrationTest extends BaseIntegrationTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0.0, Status.BAD_REQUEST},
                {-1.0, Status.BAD_REQUEST},
                {0.01, Status.OK},
                {1_999_999_999.99, Status.OK},
                {2_000_000_000, Status.BAD_REQUEST}
        });
    }

    private int id;
    private int currencyId;
    private double balance;
    private Status status;

    public AccountBalanceValuesIntegrationTest(double balance, Status status) {
        this.balance = balance;
        this.status = status;
    }

    @Before
    public void setUp() throws Exception {
        id = 1;
        currencyId = 2;
        AccountClientUtil.createAccountAndVerifySuccess(id, currencyId, BigDecimal.TEN);
    }

    @After
    public void tearDown() throws Exception {
        AccountClientUtil.removeAllAccounts();
    }

    @Test
    public void updateAccountBalance() throws IOException {
        AccountClientUtil.updateAccountAndVerifyStatusCode(id, currencyId, balance, status.getCode());
    }

}
