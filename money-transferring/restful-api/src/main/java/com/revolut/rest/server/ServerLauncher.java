package com.revolut.rest.server;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.util.Optional;

public class ServerLauncher {
    public static final String BASE_URI;

    static {
        String portString = Optional.ofNullable(System.getProperty("REV_PORT")).orElse("8080");
        String hostName = Optional.ofNullable(System.getProperty("REV_HOST")).orElse("localhost");

        String basePath = "agridnev-revolut";
        int port = Integer.valueOf(portString);

        BASE_URI = "http://" + hostName + ":" + port + "/" + basePath + "/";
    }

    public static HttpServer startServer() {
        final ResourceConfig rc = new RestfulApiResourceConfig();

        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }
}
