package com.revolut.rest.api.currency.domain;

public class CrossCurrency {
    private final int fromCurrencyId;
    private final int toCurrencyId;

    public CrossCurrency(int fromCurrencyId, int toCurrencyId) {
        this.fromCurrencyId = fromCurrencyId;
        this.toCurrencyId = toCurrencyId;
    }

    public int getFromCurrencyId() {
        return fromCurrencyId;
    }

    public int getToCurrencyId() {
        return toCurrencyId;
    }

    public static CrossCurrency of(int fromCurrencyId, int toCurrencyId) {
        return new CrossCurrency(fromCurrencyId, toCurrencyId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CrossCurrency that = (CrossCurrency) o;

        if (fromCurrencyId != that.fromCurrencyId) return false;
        return toCurrencyId == that.toCurrencyId;
    }

    @Override
    public int hashCode() {
        int result = fromCurrencyId;
        result = 31 * result + toCurrencyId;
        return result;
    }
}
