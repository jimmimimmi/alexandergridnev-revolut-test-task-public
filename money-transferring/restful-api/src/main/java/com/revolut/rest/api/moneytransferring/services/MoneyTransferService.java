package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;
import com.revolut.rest.api.account.repository.AccountRepository;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.moneytransferring.dto.MoneyTransferDto;

import javax.inject.Inject;
import java.math.BigDecimal;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

public class MoneyTransferService {
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private MoneyTransferAmountValidator moneyTransferAmountValidator;
    @Inject
    private MoneyTransferCrossCurrencyRateProvider moneyTransferCrossCurrencyRateProvider;
    @Inject
    private MoneyTransferExecutor moneyTransferExecutor;


    public ExecutionResult executeMoneyTransfer(MoneyTransferDto moneyTransferDto) {
        int fromAccountId = moneyTransferDto.getFromAccountId();
        int toAccountId = moneyTransferDto.getToAccountId();
        BigDecimal amount = moneyTransferDto.getAmount();

        if (fromAccountId == toAccountId) {
            return ExecutionResult.of(BAD_REQUEST, "It is forbidden to transfer money to yourself: " + fromAccountId, fromAccountId);
        }

        Account fromAccount = accountRepository.get(fromAccountId);
        Account toAccount = accountRepository.get(toAccountId);

        if (fromAccount == null) {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + fromAccountId, fromAccountId);
        }

        if (toAccount == null) {
            return ExecutionResult.of(BAD_REQUEST, "No entity was found with ID: " + toAccountId, toAccountId);
        }

        BigDecimal crossRate = moneyTransferCrossCurrencyRateProvider.getRate(fromAccount, toAccount);
        if (crossRate == null) {
            String invalidValue = "fromCurrencyId: " + fromAccount.getCurrencyId() + ", toCurrencyId: " + toAccount.getCurrencyId();
            return ExecutionResult.of(BAD_REQUEST, "No crossRate was found for " + invalidValue, invalidValue);
        }

        Object lock1;
        Object lock2;

        if (fromAccountId < toAccountId) {
            lock1 = fromAccount;
            lock2 = toAccount;
        } else {
            lock1 = toAccount;
            lock2 = fromAccount;
        }

        synchronized (lock1) {
            synchronized (lock2) {
                if (!moneyTransferAmountValidator.isValidAmount(amount, fromAccount)) {
                    return ExecutionResult.of(BAD_REQUEST, "Transfer amount has to be positive and do not exceed account's balance: " + amount, amount);
                }

                moneyTransferExecutor.executeTransfer(fromAccount, toAccount, amount, crossRate);
            }
        }

        return ExecutionResult.SUCCESS;
    }

    void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    void setMoneyTransferAmountValidator(MoneyTransferAmountValidator moneyTransferAmountValidator) {
        this.moneyTransferAmountValidator = moneyTransferAmountValidator;
    }

    void setMoneyTransferCrossCurrencyRateProvider(MoneyTransferCrossCurrencyRateProvider moneyTransferCrossCurrencyRateProvider) {
        this.moneyTransferCrossCurrencyRateProvider = moneyTransferCrossCurrencyRateProvider;
    }

    void setMoneyTransferExecutor(MoneyTransferExecutor moneyTransferExecutor) {
        this.moneyTransferExecutor = moneyTransferExecutor;
    }
}
