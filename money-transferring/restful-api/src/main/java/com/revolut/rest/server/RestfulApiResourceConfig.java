package com.revolut.rest.server;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
public class RestfulApiResourceConfig extends ResourceConfig {

    public RestfulApiResourceConfig() {
        register(new RestfulApiDependencyBinder());
        packages("com.revolut.rest");
    }


}