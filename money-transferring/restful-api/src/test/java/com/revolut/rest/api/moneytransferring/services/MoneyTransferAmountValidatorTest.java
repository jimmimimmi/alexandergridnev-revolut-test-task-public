package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(Parameterized.class)
public class MoneyTransferAmountValidatorTest {
    @Mock
    private Account account;

    private MoneyTransferAmountValidator validator;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {-1.0, 10.0, false},
                {0.0, 10.0, false},
                {0.01, 10.0, true},
                {10.01, 10.0, false},
                {1_999_999_999.99, 1_999_999_999.99, true},
                {2_000_000_000, 1_999_999_999.99, false}
        });
    }

    private double initialBalance;
    private double amount;
    private boolean result;

    public MoneyTransferAmountValidatorTest(double amount, double initialBalance, boolean result) {
        this.initialBalance = initialBalance;
        this.amount = amount;
        this.result = result;
    }

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        validator = new MoneyTransferAmountValidator();
    }

    @Test
    public void validateAmount() {
        when(account.getBalance()).thenReturn(BigDecimal.valueOf(initialBalance));
        boolean result = validator.isValidAmount(BigDecimal.valueOf(amount), account);

        assertEquals(this.result, result);

    }
}