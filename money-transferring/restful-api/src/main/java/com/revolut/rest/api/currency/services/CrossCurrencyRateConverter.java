package com.revolut.rest.api.currency.services;

import com.revolut.rest.api.currency.domain.CrossCurrency;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;
import com.revolut.rest.api.currency.dto.CrossCurrencyRateDto;

public class CrossCurrencyRateConverter {
    public CrossCurrencyRateDto convertToDTO(CrossCurrencyRate domain) {
        CrossCurrencyRateDto dto = new CrossCurrencyRateDto();

        dto.setFromCurrencyId(domain.getFromCurrencyId());
        dto.setToCurrencyId(domain.getToCurrencyId());
        dto.setRate(domain.getRate());

        return dto;
    }

    public CrossCurrencyRate convertToDomain(CrossCurrencyRateDto dto) {
        return new CrossCurrencyRate(new CrossCurrency(dto.getFromCurrencyId(), dto.getToCurrencyId()), dto.getRate());
    }
}
