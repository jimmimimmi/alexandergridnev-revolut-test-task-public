package com.revolut.rest.api.account.repository;

import com.revolut.rest.api.account.domain.Account;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class AccountRepository {
    private final ConcurrentHashMap<Integer, Account> map = new ConcurrentHashMap<>();

    public Account add(Account account) {
        return map.putIfAbsent(account.getId(), account);
    }

    public Account get(int id) {
        return map.get(id);
    }

    public Account delete(int id) {
        return map.remove(id);
    }

    public Collection<Account> getAll() {
        return map.values();
    }
}
