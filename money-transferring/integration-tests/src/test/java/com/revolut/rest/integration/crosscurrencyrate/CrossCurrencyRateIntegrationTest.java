package com.revolut.rest.integration.crosscurrencyrate;

import com.jayway.restassured.http.ContentType;
import com.revolut.rest.integration.BaseIntegrationTest;
import com.revolut.rest.integration.testutil.CrossCurrencyRateClienUtil;
import com.revolut.rest.integration.testutil.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.Assert.assertEquals;
import static org.junit.internal.matchers.StringContains.containsString;

public class CrossCurrencyRateIntegrationTest extends BaseIntegrationTest {

    private int fromCurrencyId;
    private int toCurrencyId;
    private BigDecimal rate;

    @Before
    public void setUp() throws Exception {
        fromCurrencyId = 1;
        toCurrencyId = 2;
        rate = BigDecimal.valueOf(1.12345);
    }

    @After
    public void tearDown() throws Exception {
        CrossCurrencyRateClienUtil.deleteCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId);
    }

    @Test()
    public void crossCurrencyRateNotFound() {
        CrossCurrencyRateClienUtil.getCrossCurrencyRateResponse(-1, -2)
                .statusCode(Status.NOT_FOUND.getCode())
                .contentType(ContentType.JSON)
                .body(containsString("No entity was found with ID: [-1, -2]"));
    }

    @Test
    public void createCrossCurrencyRate() throws IOException {
        CrossCurrencyRateClienUtil.createCrossCurrencyRateAndAssert(fromCurrencyId, toCurrencyId, rate);
    }

    @Test
    public void updateCrossCurrencyRate() throws IOException {
        CrossCurrencyRateClienUtil.createCrossCurrencyRateAndAssert(fromCurrencyId, toCurrencyId, rate);

        CrossCurrencyRateClienUtil.createCrossCurrencyRateAndAssert(fromCurrencyId, toCurrencyId, BigDecimal.valueOf(10.11111));
    }

    @Test
    public void deleteCrossCurrencyRate() throws IOException {
        CrossCurrencyRateClienUtil.createCrossCurrencyRateAndAssert(fromCurrencyId, toCurrencyId, rate);

        CrossCurrencyRateClienUtil.deleteCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId)
                .statusCode(Status.OK.getCode());

        CrossCurrencyRateClienUtil.getCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId)
                .statusCode(Status.NOT_FOUND.getCode())
                .contentType(ContentType.JSON)
                .body(containsString("No entity was found with ID: [" + fromCurrencyId + ", " + toCurrencyId + "]"));

    }

    @Test
    public void canNotDeleteNonExistentCrossCurrencyRate() throws IOException {
        toCurrencyId = -1;
        CrossCurrencyRateClienUtil.deleteCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId)
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString(
                        "No entity was found with ID: " + "[" + fromCurrencyId + ", " + toCurrencyId + "]"));
    }

    @Test
    public void canNotCreateCrossCurrencyRateForNonExistentCurrency() throws IOException {
        toCurrencyId = -1;
        CrossCurrencyRateClienUtil.createCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId, rate)
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString(
                        "No entity was found with ID: " + toCurrencyId));
    }

    @Test
    public void canNotCreateCrossCurrencyRateForTheSameCurrency() throws IOException {
        toCurrencyId = fromCurrencyId;
        CrossCurrencyRateClienUtil.createCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId, rate)
                .statusCode(Status.BAD_REQUEST.getCode())
                .body(containsString(
                        "It is forbidden to create cross rate for the same currency"));
    }

}
