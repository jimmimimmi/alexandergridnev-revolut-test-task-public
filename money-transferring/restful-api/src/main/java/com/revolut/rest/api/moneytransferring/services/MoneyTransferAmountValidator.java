package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;

import java.math.BigDecimal;

public class MoneyTransferAmountValidator {
    public static final int MIN_VALUE_EXCLUSIVE = 0;
    public static final int MAX_VALUE_EXCLUSIVE = 2_000_000_000;

    public boolean isValidAmount(BigDecimal amount, Account account) {
        int comparingWithMinValue = amount.compareTo(BigDecimal.valueOf(MIN_VALUE_EXCLUSIVE));
        int comparingWithMaxValue = amount.compareTo(BigDecimal.valueOf(MAX_VALUE_EXCLUSIVE));
        int comparingWithAvailableAmount = amount.compareTo(account.getBalance());

        return comparingWithMinValue > 0 &&
                comparingWithMaxValue < 0 &&
                comparingWithAvailableAmount <= 0;
    }
}
