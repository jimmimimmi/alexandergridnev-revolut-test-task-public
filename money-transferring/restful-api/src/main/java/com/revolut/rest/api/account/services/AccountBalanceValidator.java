package com.revolut.rest.api.account.services;

import java.math.BigDecimal;

public class AccountBalanceValidator {
    public static final int MIN_VALUE_EXCLUSIVE = 0;
    public static final int MAX_VALUE_EXCLUSIVE = 2_000_000_000;

    public boolean isValidBalance(BigDecimal rate) {
        int comparingWithMinValue = rate.compareTo(BigDecimal.valueOf(MIN_VALUE_EXCLUSIVE));
        int comparingWithMaxValue = rate.compareTo(BigDecimal.valueOf(MAX_VALUE_EXCLUSIVE));
        return comparingWithMinValue > 0 && comparingWithMaxValue < 0;
    }
}
