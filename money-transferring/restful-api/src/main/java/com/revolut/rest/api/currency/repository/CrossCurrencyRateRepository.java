package com.revolut.rest.api.currency.repository;

import com.revolut.rest.api.currency.domain.CrossCurrency;
import com.revolut.rest.api.currency.domain.CrossCurrencyRate;

import java.util.concurrent.ConcurrentHashMap;

public class CrossCurrencyRateRepository {
    private final ConcurrentHashMap<CrossCurrency, CrossCurrencyRate> map = new ConcurrentHashMap<>();

    public CrossCurrencyRate get(int fromCurrencyId, int toCurrencyId) {
        return map.get(CrossCurrency.of(fromCurrencyId, toCurrencyId));
    }

    public void add(CrossCurrencyRate crossCurrencyRate) {
        map.put(crossCurrencyRate.getCrossCurrency(), crossCurrencyRate);
    }

    public CrossCurrencyRate delete(CrossCurrency crossCurrency) {
        return map.remove(crossCurrency);
    }
}
