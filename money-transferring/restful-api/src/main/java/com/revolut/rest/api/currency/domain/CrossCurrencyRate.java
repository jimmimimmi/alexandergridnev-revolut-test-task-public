package com.revolut.rest.api.currency.domain;

import java.math.BigDecimal;

public class CrossCurrencyRate {
    private final CrossCurrency crossCurrency;

    private final BigDecimal rate;

    public CrossCurrencyRate(CrossCurrency crossCurrency, BigDecimal rate) {
        assert crossCurrency != null;

        this.crossCurrency = crossCurrency;
        this.rate = rate;
    }

    public int getFromCurrencyId() {
        return crossCurrency.getFromCurrencyId();
    }

    public int getToCurrencyId() {
        return crossCurrency.getToCurrencyId();
    }

    public BigDecimal getRate() {
        return rate;
    }

    public CrossCurrency getCrossCurrency() {
        return crossCurrency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CrossCurrencyRate that = (CrossCurrencyRate) o;

        return crossCurrency.equals(that.crossCurrency);
    }

    @Override
    public int hashCode() {
        return crossCurrency.hashCode();
    }
}
