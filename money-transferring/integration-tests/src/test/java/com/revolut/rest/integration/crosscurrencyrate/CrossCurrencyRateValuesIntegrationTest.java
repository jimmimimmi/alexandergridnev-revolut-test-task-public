package com.revolut.rest.integration.crosscurrencyrate;

import com.revolut.rest.integration.BaseIntegrationTest;
import com.revolut.rest.integration.testutil.CrossCurrencyRateClienUtil;
import com.revolut.rest.integration.testutil.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CrossCurrencyRateValuesIntegrationTest extends BaseIntegrationTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0.0, Status.BAD_REQUEST},
                {-1.0, Status.BAD_REQUEST},
                {0.01, Status.OK},
                {1_999_999_999.99, Status.OK},
                {2_000_000_000, Status.BAD_REQUEST}
        });
    }

    private int fromCurrencyId;
    private int toCurrencyId;
    private double rate;
    private Status status;

    public CrossCurrencyRateValuesIntegrationTest(double rate, Status status) {
        this.rate = rate;
        this.status = status;
    }

    @Before
    public void setUp() throws Exception {
        fromCurrencyId = 1;
        toCurrencyId = 2;
    }

    @After
    public void tearDown() throws Exception {
        CrossCurrencyRateClienUtil.deleteCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId);
    }

    @Test
    public void createCrossCurrencyRate() throws IOException {
        CrossCurrencyRateClienUtil
                .createCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId, BigDecimal.valueOf(rate))
                .statusCode(this.status.getCode());
    }
}
