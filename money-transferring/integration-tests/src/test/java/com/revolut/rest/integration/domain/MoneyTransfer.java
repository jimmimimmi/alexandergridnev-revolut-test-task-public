package com.revolut.rest.integration.domain;

import java.math.BigDecimal;

public class MoneyTransfer {
    private int fromAccountId;
    private int toAccountId;
    private BigDecimal amount;

    public int getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(int fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public int getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(int toAccountId) {
        this.toAccountId = toAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public static MoneyTransfer of(int fromAccountId, int toAccountId, BigDecimal amount) {
        MoneyTransfer moneyTransfer = new MoneyTransfer();

        moneyTransfer.setFromAccountId(fromAccountId);
        moneyTransfer.setToAccountId(toAccountId);
        moneyTransfer.setAmount(amount);

        return moneyTransfer;
    }
}
