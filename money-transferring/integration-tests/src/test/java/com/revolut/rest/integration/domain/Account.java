package com.revolut.rest.integration.domain;

import java.math.BigDecimal;

public class Account {
    private int id;
    private int currencyId;
    private BigDecimal balance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public static Account of(int id, int currencyId, BigDecimal balance) {
        Account account = new Account();

        account.setBalance(balance);
        account.setId(id);
        account.setCurrencyId(currencyId);

        return account;
    }
}
