# Alexander Gridnev. Revolut money transferring test task

## Running service


In order to build the project you need to use MAVEN 
```bash
mvn clean install 
```
The service is hosted on `http://localhost:8080` by default. 


However you can specify host (`REV_HOST` parameter) and/or port (`REV_PORT` parameter) explicitly.
```bash
java -jar restful-api/target/restful-api-1.0-SNAPSHOT.jar

java -jar -DREV_PORT=YOURPORT -DREV_HOST=YOURHOST restful-api/target/restful-api-1.0-SNAPSHOT.jar
```
## Testing
Integration tests are located in `/integration-tests`. The restful service must be launched. The simplest way to run tests is MAVEN:
```bash
mvn clean install -DskipITs=false 

mvn clean install -DskipITs=false -DREV_PORT=YOURPORT
```

## API

There are 4 entities which the service operates:

**1. Currency**. It has only id. For sake of simplicity the system has a predefined set of currencies: 
```bash
[{id: 1}, {id: 2}, {id: 3}]
```
**2. Account**. `{id:1, currencyId: 2, balance: 100}`

- get all accounts. (`GET /account/all`). The result is json array: 
```bash
[{id:1, currencyId: 2, balance: 100}, {id:2, currencyId: 2, balance: 200}]
```
- get details of one particular account by id (`GET /account/id`). The result is found account
```bash
{id:1, currencyId: 2, balance: 100}
```
- delete account by id (`DELETE /account/id`). 
- create new account  (`POST /account  {id:1, currencyId: 2, balance: 100}`). 
- update account  (`PUT /account  {id:1, currencyId: 2, balance: 100}`). 


**3. CrossCurrencyRate**. `{fromCurrencyId:1, toCurrencyId: 2, rate: 100}`

- get rate for particular pair of currencies (`GET /cross-rates?fromCurrencyId=1&toCurrencyId=2`). 
- delete rate for particular pair of currencies (`DELETE /cross-rates?fromCurrencyId=1&toCurrencyId=2`). 
- create or update rate for particular pair of currencies (`POST /cross-rates  {fromCurrencyId:1, toCurrencyId: 2, rate: 100}`). 

**4. MoneyTransfer**. `{fromAccountId:1, toAccountId: 2, amount: 100}`

- create new money transfer (`POST /transfer  {fromAccountId:1, toAccountId: 2, amount: 100}`).
