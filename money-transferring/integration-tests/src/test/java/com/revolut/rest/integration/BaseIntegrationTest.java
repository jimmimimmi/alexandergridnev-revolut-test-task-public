package com.revolut.rest.integration;

import com.jayway.restassured.RestAssured;
import org.junit.Before;

import java.util.Optional;


public abstract class BaseIntegrationTest {

    private static final int PORT;
    private static final String BASE_HOST;

    static {
        String portString = Optional.ofNullable(System.getProperty("REV_PORT")).orElse("8080");
        String hostName = Optional.ofNullable(System.getProperty("REV_HOST")).orElse("localhost");

        PORT = Integer.valueOf(portString);
        BASE_HOST = "http://" + hostName;
    }

    @Before
    public void setUpBaseIntegrationTest() throws Exception {
        RestAssured.port = PORT;
        RestAssured.baseURI = BASE_HOST;
    }
}
