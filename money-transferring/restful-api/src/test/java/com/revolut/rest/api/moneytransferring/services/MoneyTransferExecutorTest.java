package com.revolut.rest.api.moneytransferring.services;

import com.revolut.rest.api.account.domain.Account;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MoneyTransferExecutorTest {
    @Mock
    private Account fromAccount;
    @Mock
    private Account toAccount;

    private BigDecimal fromAccountBalance;
    private BigDecimal toAccountBalance;
    private BigDecimal transferAmount;
    private BigDecimal crossCurrencyRate;

    private MoneyTransferExecutor moneyTransferExecutor;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        fromAccountBalance = BigDecimal.valueOf(123456);
        toAccountBalance = BigDecimal.valueOf(456121);
        transferAmount = BigDecimal.valueOf(1000);
        crossCurrencyRate = BigDecimal.valueOf(1.5);

        when(fromAccount.getBalance()).thenReturn(fromAccountBalance);
        when(toAccount.getBalance()).thenReturn(toAccountBalance);

        moneyTransferExecutor = new MoneyTransferExecutor();
    }

    @Test
    public void transfer() {
        moneyTransferExecutor.executeTransfer(fromAccount, toAccount, transferAmount, crossCurrencyRate);

        BigDecimal newBalanceForFromAccount = fromAccountBalance.subtract(transferAmount);
        BigDecimal newBalanceForToAccount = toAccountBalance.add(transferAmount.multiply(crossCurrencyRate));

        verify(fromAccount).setBalance(newBalanceForFromAccount);
        verify(toAccount).setBalance(newBalanceForToAccount);
    }
}