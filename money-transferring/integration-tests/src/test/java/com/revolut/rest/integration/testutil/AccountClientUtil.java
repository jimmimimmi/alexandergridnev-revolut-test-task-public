package com.revolut.rest.integration.testutil;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import com.revolut.rest.integration.domain.Account;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.Assert.assertEquals;

public class AccountClientUtil {

    public static void updateAccountAndVerifyStatusCode(int id, int currencyId, double balance, int statusCode) {
        updateAccountResponse(id, currencyId, BigDecimal.valueOf(balance))
                .statusCode(statusCode);
    }

    public static List<Account> getAllAccounts() throws IOException {
        RestAssured.basePath = RestfulApiEndPoints.ACCOUNT;
        ObjectMapper mapper = new ObjectMapper();
        String allAccountsJson =
                given()
                        .when()
                        .get("/all")
                        .then()
                        .statusCode(Status.OK.getCode())
                        .contentType(ContentType.JSON)
                        .extract()
                        .response()
                        .asString();

        return mapper.readValue(allAccountsJson,
                new TypeReference<List<Account>>() {
                });
    }


    public static void removeAllAccounts() throws IOException {
        List<Account> allAccounts = getAllAccounts();
        for (Account account : allAccounts) {
            deleteAccountAndVerifySuccess(account);
        }
    }

    public static ValidatableResponse getAccountResponse(int id) {
        RestAssured.basePath = RestfulApiEndPoints.ACCOUNT;
        return given()
                .when()
                .accept(ContentType.JSON)
                .pathParam("id", id)
                .get("/{id}")
                .then();
    }

    public static ValidatableResponse deleteAccountResponse(int id) {
        RestAssured.basePath = RestfulApiEndPoints.ACCOUNT;

        return given()
                .when()
                .accept(ContentType.JSON)
                .pathParam("id", id)
                .delete("/{id}")
                .then();
    }

    public static void deleteAccountAndVerifySuccess(Account account) {
        deleteAccountResponse(account.getId()).statusCode(Status.OK.getCode());
    }

    public static Account getAccount(int id) throws IOException {
        String createdAccountJSON =
                getAccountResponse(id)
                        .statusCode(Status.OK.getCode())
                        .contentType(ContentType.JSON)
                        .extract()
                        .body()
                        .asString();

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(createdAccountJSON, Account.class);
    }

    public static ValidatableResponse updateAccountResponse(int id, int currencyId, BigDecimal balance) {
        RestAssured.basePath = RestfulApiEndPoints.ACCOUNT;

        return given().contentType("application/json")
                .accept(ContentType.JSON)
                .body(Account.of(id, currencyId, balance))
                .when()
                .put()
                .then();
    }

    public static ValidatableResponse createAccountAndGetResponse(int id, int currencyId, BigDecimal balance) {
        RestAssured.basePath = RestfulApiEndPoints.ACCOUNT;

        return given().contentType("application/json")
                .accept(ContentType.JSON)
                .body(Account.of(id, currencyId, balance))
                .when()
                .post()
                .then();
    }

    public static void createAccountAndVerifySuccess(int id, int currencyId, BigDecimal balance) {
        createAccountAndGetResponse(id, currencyId, balance)
                .statusCode(Status.OK.getCode());
    }

    public static void getAccountAndAssert(int id, int currencyId, BigDecimal balance) throws IOException {
        Account account = getAccount(id);

        assertEquals(id, account.getId());
        assertEquals(currencyId, account.getCurrencyId());
        assertEquals(balance, account.getBalance());
    }

    public static void updateAccountAndAssert(int id, int currencyId, BigDecimal balance) throws IOException {
        updateAccountResponse(id, currencyId, balance)
                .statusCode(Status.OK.getCode());

        getAccountAndAssert(id, currencyId, balance);
    }

    public static void deleteAccountAndAssert(int id) {
        deleteAccountResponse(id).statusCode(Status.OK.getCode());
        getAccountResponse(id).statusCode(Status.NOT_FOUND.getCode());
    }

    public static void createAccountAndAssert(int id, int currencyId, BigDecimal balance) throws IOException {
        createAccountAndGetResponse(id, currencyId, balance)
                .statusCode(Status.OK.getCode());

        getAccountAndAssert(id, currencyId, balance);
    }
}
