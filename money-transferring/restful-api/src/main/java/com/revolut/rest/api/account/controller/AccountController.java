package com.revolut.rest.api.account.controller;

import com.revolut.rest.api.account.dto.AccountDto;
import com.revolut.rest.api.account.services.AccountService;
import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.common.ExecutionResult;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/account")
public class AccountController {

    @Inject
    private AccountService accountService;
    @Inject
    private ClientExceptionFactory clientExceptionFactory;

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AccountDto> getAllAccounts() {
        return accountService.getAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@PathParam("id") int id) {
        AccountDto accountDto = accountService.get(id);

        if (accountDto != null) {
            return Response.ok(accountDto).build();
        } else {
            ExecutionResult result =
                    ExecutionResult.of(Response.Status.NOT_FOUND, "No entity was found with ID: " + id, id);
            throw clientExceptionFactory.create(result);
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(AccountDto accountDto) {
        ExecutionResult result = accountService.create(accountDto);

        if (result == ExecutionResult.SUCCESS) {
            return Response.ok(result).build();
        } else {
            throw clientExceptionFactory.create(result);
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAccount(AccountDto accountDto) {
        ExecutionResult result = accountService.update(accountDto);

        if (result == ExecutionResult.SUCCESS) {
            return Response.ok(result).build();
        } else {
            throw clientExceptionFactory.create(result);
        }
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccount(@PathParam("id") int id) {
        ExecutionResult result = accountService.delete(id);

        if (result == ExecutionResult.SUCCESS) {
            return Response.ok(result).build();
        } else {
            throw clientExceptionFactory.create(result);
        }
    }

    public AccountController setAccountService(AccountService accountService) {
        this.accountService = accountService;
        return this;
    }

    public AccountController setClientExceptionFactory(ClientExceptionFactory clientExceptionFactory) {
        this.clientExceptionFactory = clientExceptionFactory;
        return this;
    }
}
