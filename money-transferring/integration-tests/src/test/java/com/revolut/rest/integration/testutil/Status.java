package com.revolut.rest.integration.testutil;

public enum Status {
    OK(200),
    BAD_REQUEST(400),
    NOT_FOUND(404);

    private final int code;

    Status(int statusCode) {
        code = statusCode;
    }

    public int getCode() {
        return code;
    }

}
