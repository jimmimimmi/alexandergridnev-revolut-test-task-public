package com.revolut.rest.api.currency.controller;

import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.common.ExecutionResult;
import com.revolut.rest.api.currency.dto.CrossCurrencyRateDto;
import com.revolut.rest.api.currency.services.CrossCurrencyRateService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Path("/cross-rates")
public class CrossCurrencyRateController {

    @Inject
    private CrossCurrencyRateService crossCurrencyRateService;
    @Inject
    private ClientExceptionFactory clientExceptionFactory;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCrossCurrencyRate(@QueryParam("fromCurrencyId") int fromCurrencyId,
                                         @QueryParam("toCurrencyId") int toCurrencyId) {
        CrossCurrencyRateDto crossCurrencyRate = crossCurrencyRateService.get(fromCurrencyId, toCurrencyId);

        if (crossCurrencyRate != null) {
            return Response.ok(crossCurrencyRate).build();
        } else {
            String invalidValue = "[" + fromCurrencyId + ", " + toCurrencyId + "]";
            ExecutionResult result =
                    ExecutionResult.of(Response.Status.NOT_FOUND, "No entity was found with ID: " + invalidValue, invalidValue);
            throw clientExceptionFactory.create(result);
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createOrUpdateCrossCurrencyRate(CrossCurrencyRateDto crossCurrencyRateDto) {
        ExecutionResult result = crossCurrencyRateService.createOrUpdate(crossCurrencyRateDto);

        if (result == ExecutionResult.SUCCESS) {
            return Response.ok(result).build();
        } else {
            throw clientExceptionFactory.create(result);
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCrossCurrencyRate(@QueryParam("fromCurrencyId") int fromCurrencyId,
                                            @QueryParam("toCurrencyId") int toCurrencyId) {
        ExecutionResult result = crossCurrencyRateService.delete(fromCurrencyId, toCurrencyId);

        if (result == ExecutionResult.SUCCESS) {
            return Response.ok(result).build();
        } else {
            throw clientExceptionFactory.create(result);
        }
    }

    void setCrossCurrencyRateService(CrossCurrencyRateService crossCurrencyRateService) {
        this.crossCurrencyRateService = crossCurrencyRateService;
    }

    void setClientExceptionFactory(ClientExceptionFactory clientExceptionFactory) {
        this.clientExceptionFactory = clientExceptionFactory;
    }
}
