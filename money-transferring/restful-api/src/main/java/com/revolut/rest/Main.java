package com.revolut.rest;

import com.revolut.rest.server.ServerLauncher;
import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        final HttpServer server = ServerLauncher.startServer();
        System.out.println("\n=================\n");
        System.out.println(
                "Alexander Gridnev revolut test app available at \n" +
                        ServerLauncher.BASE_URI +
                        "\nPress enter to stop it...");

        System.in.read();
        server.stop();
    }
}
