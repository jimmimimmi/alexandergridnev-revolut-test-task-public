package com.revolut.rest.api.currency.repository;

import com.revolut.rest.api.currency.domain.Currency;

import java.util.concurrent.ConcurrentHashMap;

//@Provider
//@Singleton
public class CurrencyRepository {
    private final ConcurrentHashMap<Integer, Currency> map = new ConcurrentHashMap<>();

    public Currency getById(int id) {
        return map.get(id);
    }

    public void add(Currency currency) {
        map.putIfAbsent(currency.getId(), currency);
    }
}
