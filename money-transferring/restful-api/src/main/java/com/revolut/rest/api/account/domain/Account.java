package com.revolut.rest.api.account.domain;

import java.math.BigDecimal;

public class Account {
    private final int id;
    private final int currencyId;
    private volatile BigDecimal balance;

    public Account(int id, int currencyId, BigDecimal balance) {
        this.id = id;
        this.currencyId = currencyId;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
