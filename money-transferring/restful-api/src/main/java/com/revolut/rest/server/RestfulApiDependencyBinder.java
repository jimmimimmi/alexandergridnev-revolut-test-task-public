package com.revolut.rest.server;

import com.revolut.rest.api.account.controller.AccountController;
import com.revolut.rest.api.account.repository.AccountRepository;
import com.revolut.rest.api.account.services.AccountBalanceValidator;
import com.revolut.rest.api.account.services.AccountConverter;
import com.revolut.rest.api.account.services.AccountService;
import com.revolut.rest.api.common.ClientExceptionFactory;
import com.revolut.rest.api.currency.domain.Currency;
import com.revolut.rest.api.currency.repository.CrossCurrencyRateRepository;
import com.revolut.rest.api.currency.repository.CurrencyRepository;
import com.revolut.rest.api.currency.services.CrossCurrencyRateConverter;
import com.revolut.rest.api.currency.services.CrossCurrencyRateService;
import com.revolut.rest.api.currency.services.CrossCurrencyRateValidator;
import com.revolut.rest.api.moneytransferring.services.MoneyTransferAmountValidator;
import com.revolut.rest.api.moneytransferring.services.MoneyTransferCrossCurrencyRateProvider;
import com.revolut.rest.api.moneytransferring.services.MoneyTransferExecutor;
import com.revolut.rest.api.moneytransferring.services.MoneyTransferService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;

public class RestfulApiDependencyBinder extends AbstractBinder {
    @Override
    protected void configure() {
        createAndBindPredefinedCurrencyRepository();

        bindCrossCurrencyRateClasses();
        bindAccountClasses();
        bindMoneyTransferClasses();
    }

    private void bindMoneyTransferClasses() {
        bind(MoneyTransferCrossCurrencyRateProvider.class).to(MoneyTransferCrossCurrencyRateProvider.class).in(Singleton.class);
        bind(MoneyTransferAmountValidator.class).to(MoneyTransferAmountValidator.class).in(Singleton.class);
        bind(MoneyTransferExecutor.class).to(MoneyTransferExecutor.class).in(Singleton.class);
        bind(MoneyTransferService.class).to(MoneyTransferService.class).in(Singleton.class);
    }

    private void bindAccountClasses() {
        bind(AccountRepository.class).to(AccountRepository.class).in(Singleton.class);
        bind(AccountBalanceValidator.class).to(AccountBalanceValidator.class).in(Singleton.class);
        bind(AccountConverter.class).to(AccountConverter.class).in(Singleton.class);
        bind(AccountService.class).to(AccountService.class).in(Singleton.class);
        bind(AccountController.class).to(AccountController.class).in(Singleton.class);
    }

    private void bindCrossCurrencyRateClasses() {
        bind(CrossCurrencyRateConverter.class).to(CrossCurrencyRateConverter.class).in(Singleton.class);
        bind(CrossCurrencyRateRepository.class).to(CrossCurrencyRateRepository.class).in(Singleton.class);
        bind(CrossCurrencyRateService.class).to(CrossCurrencyRateService.class).in(Singleton.class);
        bind(ClientExceptionFactory.class).to(ClientExceptionFactory.class).in(Singleton.class);
        bind(CrossCurrencyRateValidator.class).to(CrossCurrencyRateValidator.class).in(Singleton.class);
    }

    private void createAndBindPredefinedCurrencyRepository() {
        CurrencyRepository currencyRepository = new CurrencyRepository();

        currencyRepository.add(Currency.of(1));
        currencyRepository.add(Currency.of(2));
        currencyRepository.add(Currency.of(3));

        bind(currencyRepository).to(CurrencyRepository.class);
    }
}