package com.revolut.rest.integration.testutil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import com.revolut.rest.integration.domain.CrossCurrencyRate;

import java.io.IOException;
import java.math.BigDecimal;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.Assert.assertEquals;

public class CrossCurrencyRateClienUtil {


    public static ValidatableResponse getCrossCurrencyRateResponse(int fromCurrencyId, int toCurrencyId) {
        RestAssured.basePath = RestfulApiEndPoints.CROSS_RATES;
        return given()
                .when()
                .accept(ContentType.JSON)
                .get("?fromCurrencyId={fromCurrencyId}&toCurrencyId={toCurrencyId}", fromCurrencyId, toCurrencyId)
                .then();
    }

    public static CrossCurrencyRate getCrossCurrencyRate(int fromCurrencyId, int toCurrencyId) throws IOException {
        String createdCrossCurrencyRateJSON = getCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId)
                .statusCode(Status.OK.getCode())
                .contentType(ContentType.JSON)
                .extract()
                .body()
                .asString();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(createdCrossCurrencyRateJSON, CrossCurrencyRate.class);
    }

    public static ValidatableResponse createCrossCurrencyRateResponse(int fromCurrencyId, int toCurrencyId, BigDecimal rate) {
       RestAssured.basePath = RestfulApiEndPoints.CROSS_RATES;

        return given().contentType("application/json")
                .accept(ContentType.JSON)
                .body(CrossCurrencyRate.of(fromCurrencyId, toCurrencyId, rate))
                .when()
                .post()
                .then();
    }

    public static void createCrossCurrencyRateAndAssert(int fromCurrencyId, int toCurrencyId, BigDecimal rate) throws IOException {
        createCrossCurrencyRateResponse(fromCurrencyId, toCurrencyId, rate)
                .statusCode(Status.OK.getCode());

        CrossCurrencyRate crossCurrencyRate = getCrossCurrencyRate(fromCurrencyId, toCurrencyId);

        assertEquals(fromCurrencyId, crossCurrencyRate.getFromCurrencyId());
        assertEquals(toCurrencyId, crossCurrencyRate.getToCurrencyId());
        assertEquals(rate, crossCurrencyRate.getRate());
    }

    public static ValidatableResponse deleteCrossCurrencyRateResponse(int fromCurrencyId, int toCurrencyId) {
        RestAssured.basePath = RestfulApiEndPoints.CROSS_RATES;

        return given()
                .when()
                .accept(ContentType.JSON)
                .delete("?fromCurrencyId={fromCurrencyId}&toCurrencyId={toCurrencyId}", fromCurrencyId, toCurrencyId)
                .then();
    }
}
